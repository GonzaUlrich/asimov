﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtrasCollider : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if(col.gameObject.name=="Enemy" && GameObject.Find("Player").GetComponent<VidaPlayer>().timer >= GameObject.Find("Player").GetComponent<VidaPlayer>().inmortalidad){
			GameObject.Find("Player").GetComponent<VidaPlayer>().currentHP-=GameObject.Find("Player").GetComponent<VidaPlayer>().DañoRecivido(GameObject.Find("Player").GetComponent<VidaPlayer>().dañoEnemy);
			GameObject.Find("Player").GetComponent<MovPlayer>().estados=1;
			GameObject.Find("Player").GetComponent<VidaPlayer>().direccionActual="atras";
		}
	}
}
