﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Futuro cambiar saltos por un array
public class MovPlayer : MonoBehaviour {

    private Vector3 moveVector= Vector3.zero;
    private CharacterController controller;
    public float speed = 22;
    public float verticalVelocity;
    public float gravity=80;
    public float timer=0f;
    public int maxNumOfJumps=2;
    public int tipoDeSalto=0;
    public int estados=0;
    private float knokbak=10;
    private float fuerzaSalto=20;
    private float airSpeed;
    private float temptimer;
    private float tiempoDeSalto=0.35f;
    private int numOfJumps;
    private bool salteAntes;
    private bool rotar=true;

    

	void Start () {
        controller= GetComponent<CharacterController>();
        airSpeed=CalcularPorcentajes(speed,75);
	}

	void Update () { 
        //Condiciones de Salto y gravedad
        
        switch(estados){
            case 0://Caminar saltar y correr
        if(controller.isGrounded){
            verticalVelocity=0;
            numOfJumps=maxNumOfJumps;
            moveVector.x = Input.GetAxis("Horizontal") * speed;
            salteAntes=false;
            timer=0;
        }
        else{
            verticalVelocity-=gravity*Time.deltaTime;
            moveVector.x = Input.GetAxis("Horizontal") * airSpeed;
            
        }
        //Donde mira
        if(moveVector.x<0 && rotar){
             transform.localRotation = Quaternion.Euler(0, 180, 0);
             rotar=false;
        }
        if(moveVector.x>0 && !rotar){
             transform.localRotation = Quaternion.Euler(0, 0, 0);
             rotar=true;
        }
        
        //salto
        if(Input.GetKey("0")){
            tipoDeSalto=0;
        }
        if(Input.GetKey("1")){
            tipoDeSalto=1;
        }
        if(Input.GetKey("2")){
            tipoDeSalto=2;
        }
        if(Input.GetKey("3") ){
            tipoDeSalto=3;
        }
        switch(tipoDeSalto){
            
            case 0: // ambos saltos son iguales dependen del tiempo que presiones
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<tiempoDeSalto && numOfJumps>0){
                salteAntes=true;
                timer+=Time.deltaTime;
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=fuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,50);
                }
            }
           
            break;

            case 1://1 salto depende de la fuerza y el segundo es fijo
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<tiempoDeSalto && numOfJumps==maxNumOfJumps){
                salteAntes=true;
                timer+=Time.deltaTime;
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=fuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,50);
                }
            }
            if((Input.GetKeyDown("w") ||Input.GetKeyDown("up")) && numOfJumps>0 && numOfJumps!=maxNumOfJumps && numOfJumps>0){
                verticalVelocity=fuerzaSalto*1.5f;
                numOfJumps-=1;
            }

            break;

            //El segundo salto es mas debil que el primero
            case 2:
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<tiempoDeSalto && numOfJumps==maxNumOfJumps){
                salteAntes=true;
                timer+=Time.deltaTime;
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=fuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,50);
                }
            }
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<tiempoDeSalto && numOfJumps!=maxNumOfJumps && numOfJumps>0){
                salteAntes=true;
                timer+=Time.deltaTime;
                float tempfuerzaSalto=CalcularPorcentajes(fuerzaSalto,75);
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=tempfuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(tempfuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(tempfuerzaSalto,50);
                }
            }
            break;

            case 3:// depende el 1er salto el segundo puede ser igual o menor
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<tiempoDeSalto && numOfJumps==maxNumOfJumps){
                salteAntes=true;
                timer+=Time.deltaTime;
                temptimer= timer;
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=fuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,50);
                }
            }
            if((Input.GetKey("w") ||Input.GetKey("up")) && timer<temptimer && numOfJumps!=maxNumOfJumps && numOfJumps>0){
                salteAntes=true;
                timer+=Time.deltaTime;
                if(timer<CalcularPorcentajes(tiempoDeSalto,50)){
                verticalVelocity=fuerzaSalto;
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,50) && timer<CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,75);
                }
                if(timer>CalcularPorcentajes(tiempoDeSalto,75)){
                verticalVelocity=CalcularPorcentajes(fuerzaSalto,50);
                }
            }
            break;
        }

        if(Input.GetKeyUp("w")||Input.GetKeyUp("up")){
            numOfJumps-=1;
            timer=0;
        }   
        //Resta 1 salto si se tira al vacio 
        if(!controller.isGrounded&&!salteAntes){
            numOfJumps-=1;
            salteAntes=true;
        }
        //Movimiento derecha izquierda
      
            break;
            
        case 1:
            timer=0;
            estados=2;
        break;

        case 2:// KNOCKBACK
        timer+=Time.deltaTime;
        
        if(GameObject.Find("Player").GetComponent<VidaPlayer>().direccionActual=="pies" || GameObject.Find("Player").GetComponent<VidaPlayer>().direccionActual=="adelante" ){
            if(controller.isGrounded && timer>0.5f){
            verticalVelocity=0;
            knokbak=10;
            moveVector.x=0;
            estados=0;
        }
         if(rotar)
        moveVector.x=-10;
        if(!rotar)
        moveVector.x=10;
        knokbak-=Time.deltaTime*40;
        verticalVelocity=knokbak;
        }
        if(GameObject.Find("Player").GetComponent<VidaPlayer>().direccionActual=="atras" ){
            if(controller.isGrounded && timer>0.5f){
            verticalVelocity=0;
            knokbak=10;
            moveVector.x=0;
            estados=0;
        }
        if(rotar)
        moveVector.x=10;
        if(!rotar)
        moveVector.x=-10;
        knokbak-=Time.deltaTime*40;
        verticalVelocity=knokbak;
        }
        
        
        break;

        }
        
        moveVector.y=verticalVelocity;
        controller.Move(moveVector*Time.deltaTime);  
    }
    float CalcularPorcentajes(float numero, float porcentaje){
        float resultado = (porcentaje*numero)/100;
        return resultado;
    }
}
