﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cabeza : MonoBehaviour {

	MovPlayer jugador;
	public int gravedadChoque=0; 

	void Start(){
		jugador=FindObjectOfType<MovPlayer>();
	}

	void OnTriggerStay(Collider col){
		if(col.name!="Player"){
			jugador.verticalVelocity=gravedadChoque;
			jugador.timer=1;// esto hace que salga del salto
		}
	}
}