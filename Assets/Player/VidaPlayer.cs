﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaPlayer : MonoBehaviour {

	public GameObject player;
	public Image frontVida;
	public float currentHP;
	public float timer= 0; 
	public float inmortalidad=3;
	public float maxHP=50;
	public float dañoEnemy=5;
	public string direccionActual;
	private float armor=4;
	private float dañoTotal;

	void Start(){
		currentHP=maxHP;
		player= GameObject.Find("Player");
		
	}

	void Update(){
		Debug.Log(currentHP);
		if(timer<inmortalidad)
		timer+=Time.deltaTime;
		if(currentHP<=0){
			player.SetActive(false);
		}	
		frontVida.fillAmount=currentHP/maxHP;
	} 

	public float DañoRecivido(float daño){
		dañoTotal=((100-armor)*daño)/100;
		timer=0;
		return dañoTotal;
	} 

	public void EstaVivo(){
		if(currentHP<=0){
			player.SetActive(false);
		}	
	}
}