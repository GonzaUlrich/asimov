﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuInicial : MonoBehaviour {

	private GameObject MenuOpciones;

	void Start(){
		MenuOpciones= GameObject.Find("MenuOpciones");
		MenuOpciones.SetActive(false);
	}

	public void NuevoJuego(){
		SceneManager.LoadScene("Main");
	}
	public void CargarJuego(){

	}
	public void Opciones (){
		MenuOpciones.SetActive(true);
	}
	public void Salir(){
		Application.Quit ();
	}
}
